# MapTool Rust Add-on Demo

This is a Demo Add-on built using rust's emscripten backend,
to demonstrate how libraries written in rust can be used in MapTool.

## Usage

Add this add-on like any other, and on-init it will open a dialogue.

## Build Instructions

To install dependencies:

```bash
apt install npm build-essential
rustup target add wasm32-unknown-unknown --toolchain stable
cargo install wasm-pack
```

To build the library:

```bash
make PROFILE=release
```

This should produce `com.gitlab.richardmaw.mt-rust-demo.mtlib` 
which you can then add to MapTool.

## How does it work?

Rust cannot be directly compiled to JavaScript
but can be compiled to Web Assembly.

MapTool uses version of JavaFX which uses a version of WebKit
that doesn't support Web Assembly,
but if a library is built using ECMAScript 6 modules
then it uses the `import` statement instead of the
`WebAssembly.instantiate` function,
and the `wasm2js` tool can turn the wasm binary into
asmJS-like JavaScript.

Because MapTool HTML panels are created by
reading the HTML returned from the macro,
setting the panel's contents to that value
and inserting some extra elements to fix URLs
rather than loading the page from a URL,
the panels have an Opaque CORS Origin,
which means that can't tell whether loading those scripts is safe
so the `import` statements fail.

This can be fixed by using a bundler to convert the scripts
into a form that works on older browsers,
which uses the old technique for loading external scripts
of inserting a new `<script>` element into the DOM.

## How do I make changes to the code?

There is significant code generation and translation,
so it is important to know which source files to edit.

The `crates/mt-demo-dialog/src/lib.rs` file is the entry-point for the rust code.

`bundle/src/hello.js` is where the rust module is loaded.
`bundle/src/hello.html` defines the HTML elements that the scripts use
and loads the script glue.
`library/mtscript/public/onInit.mts` creates the HTML dialog.
`events.json` instructs MapTool to load the `onInit` macro on load.

If the Add-On namespace is changed then
the `MTLIB_NAME` variable in the `Makefile`,
the name of the macro in `library/mtscript/public/onInit.mts` and
the `"namespace"` field of `library.json` need to be updated.

If the Rust library's name is changed then the `RSLIB_NAME` variable
in the `Makefile` and the module name in `bundle/src/hello.js`
need to be updated.

If the HTML dialogue is renamed then
the `MANIFEST.stub` rule in the `Makefile` and
the name of the macro in `library/mtscript/public/onInit.mts`
need to be updated.

## FAQ

### Why did you do this?

I am not a web developer and my JavaScript expertise is pretty basic.
TypeScript is popular for making runtime errors harder,
but if I'm going to have a build step I'd prefer a language I _am_ familiar with
and I have vague hopes of writing common rust libraries for other tools.

### Should I do this?

Probably not, native JavaScript runs faster and
you'll probably not be able to use most Rust libraries
so you'll have to write your own.

### What is this license?

It's a non-commercial code license. It is **NOT** GPL compatible.
This code exists to demonstrate how to build an add-on.

Copying the build scripts is not a GPL violation but linking the example code
with GPL code is.
It is sufficiently trivial that any significant change is a rewrite.

Using the build scripts to produce a produce a proprietary add-on would be
an AFPL violation but using them as a reference to produce your own is not.

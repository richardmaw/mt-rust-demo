# TODO: Extract this from library.json and expose it to templating
MTLIB_NAME = com.gitlab.richardmaw.mt-rust-demo
RSLIB_NAME = mt_demo_dialog
RSLIB_MACRO_NAME = mt_demo_macro

CARGO_TARGET_DIR != cargo metadata --no-deps --format-version=1 | jq -r .target_directory

PROFILE ?= dev
PROFILE_DIR_dev = debug
PROFILE_DIR_$(PROFILE) ?= $(PROFILE)
WASM_PACK_OPTS_dev = --dev
WASM_PACK_OPTS_release = --release
PARCEL_OPTS_dev = --no-optimize
PARCEL_OPTS_release =

PROFILE_TARGET_DIR = $(CARGO_TARGET_DIR)/wasm32-unknown-unknown/$(PROFILE_DIR_$(PROFILE))

all: $(MTLIB_NAME).mtlib
.PHONY: all

# Build the Rust code
$(PROFILE_TARGET_DIR)/%.wasm:
	cargo build --package=$(@:$(PROFILE_TARGET_DIR)/%.wasm=%) --target=wasm32-unknown-unknown --profile=$(PROFILE)


# Add bindings to wasm
pkg/%.js pkg/%_bg.js pkg/%_bg.wasm: $(PROFILE_TARGET_DIR)/%.wasm
	package=$(<:$(PROFILE_TARGET_DIR)/%.wasm=%); \
	package_dir="$$(dirname \
		"$$(cargo metadata --no-deps --format-version=1 | \
			jq --arg n $$package -r '.packages[] | select(.name == $$n).manifest_path' \
		)" \
	)"; \
	wasm-pack build --no-package --no-typescript \
		 --out-dir "$$(readlink -f pkg)" $(WASM_PACK_OPTS_$(PROFILE)) \
		 "$${package_dir}" --profile=$(PROFILE)


# Convert wasm to asmjs-like JavaScript and adjust bindings
bundle/src/%_bg.wasm.js: pkg/%_bg.wasm
	wasm2js -o $@ $<
bundle/src/%.js: pkg/%.js
	sed 's,_bg.wasm,&.js,g' $< >$@
bundle/src/%_bg.js: pkg/%_bg.js vendor/FastestSmallestTextEncoderDecoder-master/EncoderDecoderTogether.src.js
	cat $+ | sed 's,_bg.wasm,&.js,g' >$@


# Transpile the scripts, generating a bundle that can be loaded in MapTool
bundle/%.zip: bundle/src/%.html
	cd bundle && \
	npx parcel build $(PARCEL_OPTS_$(PROFILE)) --dist-dir .dist.$$$$/library/public $(<:bundle/%=%) && \
	(cd .dist.$$$$ && zip -r - .) >../$@; \
	ret=$$?; \
	rm -rf .dist.$$$$; \
	exit $$?
bundle/%.zip: bundle/src/%.js
	cd bundle && \
	npx parcel build $(PARCEL_OPTS_$(PROFILE)) --dist-dir .dist.$$$$/library/js $(<:bundle/%=%) && \
	(cd .dist.$$$$ && zip -r - .) >../$@; \
	ret=$$?; \
	rm -rf .dist.$$$$; \
	exit $$?

# Generate the library
%.mtlib:
	mkdir .$@.tmp.$$$$
	for z in $+; do \
		unzip $$z -d .$@.tmp.$$$$; \
	done; \
	(cd .$@.tmp.$$$$ && zip -r tmp.zip .) && \
	mv .$@.tmp.$$$$/tmp.zip $@; \
	ret=$$?; \
	rm -rf .$@.tmp.$$$$; \
	exit $$?

# TODO: The rule for generating the wasm that makes these is implicit,
# but we still need deps, maybe a function declaring a crate exists.
-include $(PROFILE_TARGET_DIR)/$(RSLIB_NAME).d
-include $(PROFILE_TARGET_DIR)/$(RSLIB_MACRO_NAME).d

# TODO: The repetition from a generated module is unfortunate, maybe a make function would help
bundle/hello.zip: bundle/src/hello.html bundle/src/hello.js bundle/src/$(RSLIB_NAME).js bundle/src/$(RSLIB_NAME)_bg.js bundle/src/$(RSLIB_NAME)_bg.wasm.js
ZIPS += bundle/hello.zip
bundle/$(RSLIB_MACRO_NAME).zip: bundle/src/$(RSLIB_MACRO_NAME).js bundle/src/$(RSLIB_MACRO_NAME)_bg.js bundle/src/$(RSLIB_MACRO_NAME)_bg.wasm.js
ZIPS += bundle/$(RSLIB_MACRO_NAME).zip

MANIFEST.in.zip: MANIFEST.in
	sed 's,.*,$@: &,' $< >MANIFEST.in.d
	zip -MM .$@.tmp.$$$$ -@ <$< && \
	mv .$@.tmp.$$$$ $@
-include MANIFEST.in.d
ZIPS += MANIFEST.in.zip

$(MTLIB_NAME).mtlib: $(ZIPS)

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = ["MapTool", "chat"])]
    fn broadcast(s: &str);
}

#[wasm_bindgen(start)]
fn run() -> Result<(), JsValue> {
    broadcast("Hello from Rust macro! You won't believe the journey it took to get here.");

    Ok(())
}

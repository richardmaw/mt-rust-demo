use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}

#[wasm_bindgen(start)]
fn run() -> Result<(), JsValue> {
    web_sys::console::log_1(&JsValue::from_str("Hello from Rust dialog!"));

    let window =
        web_sys::window().ok_or_else(|| JsValue::from_str("window global does not exist"))?;
    let document = window
        .document()
        .ok_or_else(|| JsValue::from_str("window does not have a document"))?;
    let body = document
        .body()
        .ok_or_else(|| JsValue::from_str("document does not have a body"))?;

    let node = document.create_element("p")?;
    node.set_text_content(Some("Hello from Rust!"));
    body.append_child(&node)?;

    Ok(())
}
